package cmd

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/alpacahq/alpaca-trade-api-go/alpaca"
	"github.com/guptarohit/asciigraph"
	"github.com/spf13/cobra"
	"golang.org/x/crypto/ssh/terminal"
)

// Periods of time for displaying data
const (
	PeriodDay    = "day"
	PeriodWeek   = "week"
	Period1Month = "month"
	Period3Month = "3month"
	Period1Year  = "year"
)

type barGetter interface {
	GetSymbolBars(symbol string, opts alpaca.ListBarParams) ([]alpaca.Bar, error)
}

type getCmd struct {
	cmd *cobra.Command

	validPeriods map[string]bool

	period string
}

func newGetCmd() *getCmd {
	g := &getCmd{
		cmd: &cobra.Command{
			Use:   "get SYMBOL",
			Short: "Display information on one or many stocks",
			Args:  cobra.MinimumNArgs(1),
		},
		validPeriods: map[string]bool{
			PeriodDay:    true,
			PeriodWeek:   true,
			Period1Month: true,
			Period3Month: true,
			Period1Year:  true,
		},
	}

	g.cmd.RunE = func(cmd *cobra.Command, args []string) error {
		if !g.validPeriods[g.period] {
			return fmt.Errorf("Period %s is not a valid time period", g.period)
		}

		client, err := getAlpacaClient()

		if err != nil {
			return err
		}

		return g.Get(args, client)
	}

	g.cmd.Flags().StringVarP(&g.period, "period", "p", "day", "time period of data. One of: day|week|month|3month|year")

	return g
}

func (g *getCmd) GetCommand() *cobra.Command {
	return g.cmd
}

func (g *getCmd) Get(symbols []string, getter barGetter) error {
	opts := alpaca.ListBarParams{}

	// get start and end date for bars
	var limit int
	switch g.period {
	case PeriodDay:
		opts.Timeframe = "5Min"
		limit = 75
	case PeriodWeek:
		opts.Timeframe = "15Min"
		limit = 5 * 26
	case Period1Month:
		opts.Timeframe = "1D"
		limit = 21
	case Period3Month:
		opts.Timeframe = "1D"
		limit = 62
	case Period1Year:
		opts.Timeframe = "1D"
		limit = 253
	}

	opts.Limit = &limit

	for _, symbol := range symbols {
		symbol = strings.ToUpper(symbol)
		bars, err := getter.GetSymbolBars(symbol, opts)

		if err != nil {
			return err
		} else if len(bars) == 0 {
			return fmt.Errorf("No historical data found for %s", symbol)
		}

		data := make([]float64, len(bars)+1)

		for i, bar := range bars {
			//fmt.Println(i, bar.GetTime().Format(time.RFC1123))
			data[i] = float64(bar.Open)
		}

		data[len(bars)] = float64(bars[len(bars)-1].Close)

		cap := fmt.Sprintf(
			"%s (%s - %s)",
			symbol,
			bars[0].GetTime().Format(time.ANSIC),
			bars[len(bars)-1].GetTime().Format(time.ANSIC),
		)

		opts := []asciigraph.Option{asciigraph.Caption(cap)}

		w, _, _ := terminal.GetSize(int(os.Stdout.Fd()))

		if len(bars) > w {
			opts = append(opts, asciigraph.Width(w-10))
		}

		opts = append(opts, asciigraph.Height(10))

		graph := asciigraph.Plot(data, opts...)

		fmt.Printf("📈 %s\n  $%f\n\n", symbol, bars[len(bars)-1].Close)
		fmt.Println(graph)
	}

	return nil
}
