package cmd

import (
	"fmt"
	"time"

	"github.com/alpacahq/alpaca-trade-api-go/alpaca"
	"github.com/spf13/cobra"
)

type orderLister interface {
	ListOrders(status *string, until *time.Time, limit *int, nested *bool) ([]alpaca.Order, error)
}

type ordersCmd struct {
	cmd *cobra.Command

	all    bool
	closed bool
	side   string
}

func newOrdersCmd() *ordersCmd {
	o := &ordersCmd{
		cmd: &cobra.Command{
			Use:   "orders",
			Short: "Display information on open orders",
		},
	}

	o.cmd.RunE = func(cmd *cobra.Command, args []string) error {
		client, err := getAlpacaClient()

		if err != nil {
			return err
		}

		return o.GetOrders(getTabWriter(), client)
	}

	o.cmd.Flags().BoolVarP(&o.all, "all", "a", false, "include open and closed orders. Overrides closed flag")
	o.cmd.Flags().BoolVarP(&o.closed, "closed", "c", false, "include only closed orders")
	o.cmd.Flags().StringVar(&o.side, "side", "", "one of: buy|sell")

	return o
}

func (o *ordersCmd) GetCommand() *cobra.Command {
	return o.cmd
}

func (o *ordersCmd) GetOrders(w writeFlusher, lister orderLister) error {
	status := "open"

	if o.all {
		status = "all"
	} else if o.closed {
		status = "closed"
	}

	orders, err := lister.ListOrders(&status, nil, nil, nil)

	if err != nil {
		return err
	} else if len(orders) == 0 {
		fmt.Println("No orders found")
		return nil
	}

	defer w.Flush()

	fmt.Fprintln(w, "ID\tPlaced At\tSide\tSymbol\tQty\tTiF\tLimit\tStop\tStatus\tFilled Qty\tFilled Avg\t")
	for _, order := range orders {
		if o.side != "" && o.side != string(order.Side) {
			continue
		}

		lim := "-"
		if order.LimitPrice != nil {
			lim = order.LimitPrice.StringFixed(2)
		}

		stop := "-"
		if order.StopPrice != nil {
			stop = order.StopPrice.StringFixed(2)
		}

		filledAvg := "-"
		if order.FilledAvgPrice != nil {
			filledAvg = order.FilledAvgPrice.StringFixed(2)
		}
		fmt.Fprintf(
			w,
			"%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t\n",
			order.ID,
			order.CreatedAt.Local().Format(time.RFC822),
			string(order.Side),
			order.Symbol,
			order.Qty.String(),
			string(order.TimeInForce),
			lim,
			stop,
			order.Status,
			order.FilledQty.String(),
			filledAvg,
		)
	}

	return nil
}
